resource "digitalocean_droplet" "blog" {
  image     = var.image
  name      = var.name
  region    = var.local
  size      = var.size
  ssh_keys  = var.key
  tags      = var.tags
}
resource "digitalocean_reserved_ip" "ip" {
  region     = digitalocean_droplet.blog.region
}
resource "digitalocean_reserved_ip_assignment" "ip_assign" {
  ip_address = digitalocean_reserved_ip.ip.ip_address
  droplet_id = digitalocean_droplet.blog.id
}
