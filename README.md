# digitalocean-blog
## Getting started
Create the file "token.tf", and add the following contents to it:
~~~
variable "do_token" {
  default = "your digital ocean token"
}
variable "key" {
  type    = list(string)
  default = ["your public keys from digital ocean you want to use to log into your Red Hat lab"]
}
~~~
Once built, modify the variables.tf and variables.yml files to match your needs for your blog site. 

Then, run the following commands:
~~~
terraform init
~~~
~~~
terraform plan
~~~
~~~
terraform apply
~~~
Once terraform has finished previsioning your site in Digital Ocean, 
connect the reserved ip address registered to your new Droplet in Digital 
Ocean with your domain name registrar. 

Once done, you can go see your wordpress login credentials at "/root/WO.log".
