
variable "image" {
  default = "ubuntu-22-04-x64"
}
variable "size" {
  default = "s-1vcpu-1gb"
}
variable "local" {
  default = "sfo3"  
}
variable "name" {
  default = "blog.littleseneca.com"  
}
variable "tags" {
  type    = list(string)
  default = ["littleseneca","blog"]  
}
